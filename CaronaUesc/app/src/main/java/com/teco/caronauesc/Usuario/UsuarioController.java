package com.teco.caronauesc.Usuario;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;


import java.util.List;

/**
 * Created by estagio-nit on 18/06/18.
 */

public class UsuarioController {

    public UsuarioController() {
    }

    /*Insere um usuario no Web Service*/
    public void insert(Usuario usuario, Context context) {

        try {
            boolean result = new HttpRequestAdd().execute(usuario).get();

        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }

    /*Atualiza os dados de um usuario no web Service*/
    public void upgrade(Usuario usuario, Context context) {

        try {
            boolean result = new HttpRequestUpgrade().execute(usuario).get();
        } catch (Exception e) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }

    public Usuario getUsuarioId(Integer id) {

        try {
            return new HttpRequestFind().execute(id).get();
        } catch (Exception e) {

            Log.i("Info", "Localização não encontrada");
            return null;
        }
    }

    public List<Usuario> getUsuario() {

        try {
            return new HttpRequestFindAll().execute().get();
        } catch (Exception e) {

            Log.i("Info", "usuario não encontrada");
            return null;
        }
    }


    /*classe que execulta a thread em background para o metodo get*/
    private class HttpRequestFindAll extends AsyncTask<Void, Void, List<Usuario>> {


        @Override
        protected List<Usuario> doInBackground(Void... voids) {

            UsuarioModel usuarioModel = new UsuarioModel();
            return usuarioModel.findAll();
        }

        @Override
        protected void onPostExecute(List<Usuario> list_usuarios) {
            super.onPostExecute(list_usuarios);
        }
    }


    /*classe que execulta a thread em background para o metodo get*/
    private class HttpRequestFind extends AsyncTask<Integer, Void, Usuario> {

        @Override
        protected Usuario doInBackground(Integer... params) {
            UsuarioModel usuarioModel = new UsuarioModel();
            return usuarioModel.usuarioFind(params[0]);
        }

        @Override
        protected void onPostExecute(Usuario usuario) {
            super.onPostExecute(usuario);
        }
    }


    /*class executada em background para o metodo post*/
    private class HttpRequestAdd extends AsyncTask<Usuario, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Usuario... localizacaos) {
            UsuarioModel usuarioModel = new UsuarioModel();
            return usuarioModel.insert(localizacaos[0]);
        }
    }


    /*class executada em background para o metodo post*/
    private class HttpRequestUpgrade extends AsyncTask<Usuario, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Usuario... usuarios) {
            UsuarioModel usuarioModel = new UsuarioModel();
            return usuarioModel.upgrade(usuarios[0]);
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestDelete extends AsyncTask<Usuario, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Usuario... usuarios) {
            UsuarioModel usuarioModel = new UsuarioModel();

            return usuarioModel.insert(usuarios[0]);
        }
    }
}
