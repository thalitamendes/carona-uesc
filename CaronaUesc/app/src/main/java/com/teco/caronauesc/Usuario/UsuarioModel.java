package com.teco.caronauesc.Usuario;


import android.util.Log;

import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class UsuarioModel {

    private String BASE_URL = "http://13.58.21.219/caronauescapi/usuario";
    private RestTemplate restTemplate = new RestTemplate();


    /*Método usado para retornar todos usuários*/
    public List<Usuario> findAll(){
        try{
            return restTemplate.exchange(
                    BASE_URL,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Usuario>>() {
                    }
            ).getBody();

        }catch (Exception e){
            Log.i("Erro", String.valueOf(e));
            return null;
        }
    }

    //retorna usuario de acordo com id
    public Usuario usuarioFind(int id){
        try{
            return restTemplate.exchange(
                    BASE_URL +id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Usuario>() {
                    }
            ).getBody();

        }catch (Exception e){
            return null;
        }
    }

    /*usado no metodo post*/
    public boolean insert(Usuario usuario){

        try{

            ResponseEntity<String> response = restTemplate.postForEntity(BASE_URL, returnaHttpEntity(convertUsuarioEmJsonObjeto(usuario)),String.class);

            HttpStatus status = response.getStatusCode();
            String restCall = response.getBody();

            return true;
        }catch (Exception e){
            Log.i("Erro", String.valueOf(e));
            return false;
        }
    }

    public boolean upgrade(Usuario usuario){

        try{
            restTemplate.put(BASE_URL, returnaHttpEntity(convertUsuarioEmJson(usuario)));

            return true;
        }catch (Exception e){

            return false;
        }
    }

    public String convertUsuarioEmJson(Usuario usuario){
        return   "{\"Usuario\":[{\n" +
                " \"id\": \""+usuario.getId()+"\",\n" +
                " \"nome\": \""+usuario.getNome()+"\",\n" +
                " \"email\": \""+usuario.getEmail()+"\",\n" +
                " \"senha\": \""+usuario.getSenha()+"\",\n" +
                " }]}";
    }

    public String convertUsuarioEmJsonObjeto(Usuario usuario){
        Map<String, String> values =  new HashMap<>();
        values.put("id", usuario.getId());
        values.put("nome", usuario.getNome());
        values.put("email", usuario.getEmail());
        values.put("senha", usuario.getSenha());
        JSONObject jo = new JSONObject(values);
        Log.i("Json", String.valueOf(jo));
        return String.valueOf(jo);
    }

    public HttpEntity<String> returnaHttpEntity(String json){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<String>(json, headers);
    }

}
