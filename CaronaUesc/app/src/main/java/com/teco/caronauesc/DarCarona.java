package com.teco.caronauesc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.teco.caronauesc.helper.Base64Custom;
import com.teco.caronauesc.helper.Preferencias;
import com.teco.caronauesc.model.Caronas;

import java.util.List;

public class DarCarona extends AppCompatActivity {

    private Button BotaoEnviar;
    private EditText nVagas, lBusca, hChegada, lDestino;
    private Caronas caronas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dar_carona);

        BotaoEnviar  = findViewById(R.id.BotaoEnviar);
        nVagas = findViewById(R.id.nVagas);
        lBusca = findViewById(R.id.lBusca);
        hChegada = findViewById(R.id.hChegada);
        lDestino = findViewById(R.id.lDestino);

        caronas = new Caronas();

        BotaoEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Preferencias preferencias = new Preferencias(DarCarona.this);
                String emailRecuperado = Base64Custom.codificarBase64(preferencias.getEmail());

                caronas.setNumeroVagas(nVagas.getText().toString());
                caronas.setHorarioChegada(hChegada.getText().toString());
                caronas.setLocalBusca(lBusca.getText().toString());
                caronas.setLocalDestino(lDestino.getText().toString());
                caronas.salvarCarona(emailRecuperado);

            }
        });
    }



}
