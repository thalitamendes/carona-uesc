package com.teco.caronauesc.model;

import com.google.firebase.database.DatabaseReference;
import com.teco.caronauesc.helper.ConfiguracaoFirebase;

public class Caronas {

    //Atributos das caronas
    private String numeroVagas;
    private String localBusca;
    private String localDestino;
    private String horarioChegada;

    public void salvarCarona(String email) {
        /*Recupera uma instância do Firebase e logo em seguida
        * salva os dados no nó "Caronas" criando outro nó com o email do usuário
        * convertido em base 64, cria uma chave única para cada carona para não ser
        * sobrescrita e seta o "this", ou seja, todos os atributos da classe.*/

        /*Na chamada deste método o email deve ser mudado pra ser o email de outros usuários
        * pois neste momento o aplicativo só está recuperando as caronas de quem salvou a carona
        * e não de outros usuários*/
        DatabaseReference reference = ConfiguracaoFirebase.getFirebAse();
        reference.child("Caronas").child(email).push().setValue(this);
    }

    public String getNumeroVagas() {
        return numeroVagas;
    }

    public void setNumeroVagas(String numeroVagas) {
        this.numeroVagas = numeroVagas;
    }

    public String getLocalBusca() {
        return localBusca;
    }

    public void setLocalBusca(String localBusca) {
        this.localBusca = localBusca;
    }

    public String getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(String localDestino) {
        this.localDestino = localDestino;
    }

    public String getHorarioChegada() {
        return horarioChegada;
    }

    public void setHorarioChegada(String horarioChegada) {
        this.horarioChegada = horarioChegada;
    }

    @Override
    public String toString() {
        return this.localDestino;
    }
}


