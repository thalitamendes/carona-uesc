package com.teco.caronauesc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.teco.caronauesc.helper.ConfiguracaoFirebase;

public class TelaPrincipal extends AppCompatActivity {

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

    }

    public void PedirCarona(View v){
        Intent i = new Intent(TelaPrincipal.this, PedirCarona.class);
        startActivity(i);

    }

    public void DarCarona(View v){
        Intent i = new Intent(TelaPrincipal.this, DarCarona.class);
        startActivity(i);
    }

    //Método de deslogar o usuário
    private void sair(){
        auth = ConfiguracaoFirebase.getFireBaseAuth();
        auth.signOut();
        Intent i = new Intent(TelaPrincipal.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_personalizado, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_sair:
                sair();
                break;
        }

        return true;
    }
}
