package com.teco.caronauesc.Login;

import android.util.Log;

import com.teco.caronauesc.Usuario.Usuario;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by estagio-nit on 19/06/18.
 */

public class LoginModel {
    private String BASE_URL = "http://13.58.21.219/caronauescapi/login";
    private RestTemplate restTemplate = new RestTemplate();

    /*usado no metodo post*/
    public boolean insert(Login login){

        try{

            convertLoginEmJsonObjeto(login);
            ResponseEntity<String> response = restTemplate.postForEntity(BASE_URL, returnaHttpEntity(convertLoginEmJsonObjeto(login)),String.class);

            HttpStatus status = response.getStatusCode();
            String restCall = response.getBody();
            Log.i("status", String.valueOf(status));
            Log.i("restCall", String.valueOf(restCall));

            return true;
        }catch (Exception e){
            Log.i("Erro", String.valueOf(e));
            return false;
        }
    }

    public String convertLoginEmJsonObjeto(Login login){
        Map<String, String> values =  new HashMap<>();
        values.put("email", login.getEmail());
        values.put("senha", login.getSenha());
        return String.valueOf(new JSONObject(values));
    }

    public HttpEntity<String> returnaHttpEntity(String json){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<String>(json, headers);
    }

}
