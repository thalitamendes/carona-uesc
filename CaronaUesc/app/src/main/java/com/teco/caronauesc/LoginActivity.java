package com.teco.caronauesc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.teco.caronauesc.Login.Login;
import com.teco.caronauesc.Login.LoginController;
import com.teco.caronauesc.Usuario.Usuario;
import com.teco.caronauesc.Usuario.UsuarioController;
import com.teco.caronauesc.helper.Base64Custom;
import com.teco.caronauesc.helper.ConfiguracaoFirebase;
import com.teco.caronauesc.helper.Preferencias;


public class LoginActivity extends AppCompatActivity {

    private TextView TextCadastro;

    private EditText LoginEmail, LoginSenha;
    private Button BotaoLogin;


    private DatabaseReference firebase;
    private FirebaseAuth auth;

    private Usuario usuario = new Usuario();
    private ValueEventListener valueEventListener;
    LoginController lc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        lc = new LoginController();


        //Chama o método de verificar se existe um usuário logado
        verificarUsuario();

        TextCadastro = findViewById(R.id.TextCadastro);
        BotaoLogin = findViewById(R.id.BotaoLogin);

        LoginEmail = findViewById(R.id.LoginEmail);
        LoginSenha = findViewById(R.id.LoginSenha);

        //inputEmail= findViewById(R.id.inputEmail);
        //inputSenha = findViewById(R.id.inputSenha);

        BotaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = LoginEmail.getText().toString();
                String senha = LoginSenha.getText().toString();

                if(validarLoginWS(new Login(email, senha), getBaseContext())){
                    Toast.makeText(LoginActivity.this, "Sucesso!", Toast.LENGTH_SHORT).show();
                    abrirTelaPrincipal();

                }else{
                    Toast.makeText(LoginActivity.this, "Falha ao logar!", Toast.LENGTH_SHORT).show();

                }


                // ValidarLogin();

            }
        });

        TextCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, Cadastro.class);
                startActivity(i);
            }
        });
    }

    private void ValidarLogin() {
        /*Recupera uma instancia de usuário no Firebase
        * logo em seguida cria um usuário com email e senha, e adiciona um evento que diz
        * se a tarefa foi concluída ou não*/
        auth = ConfiguracaoFirebase.getFireBaseAuth();
        auth.signInWithEmailAndPassword(usuario.getEmail(), usuario.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    /*Se a tarefa foi um sucesso, aí sim irá setar no banco de dados os atributos do usuário
                    * como email, foto, nome etc...
                    * Alguns atributos ainda devem ser adicionados em uma Activity de configurações*/

                    String usuarioEmail = Base64Custom.codificarBase64(usuario.getEmail());
                    firebase = ConfiguracaoFirebase.getFirebAse().child("Users").child(usuarioEmail);

                    valueEventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            Usuario usuarioRecuperado = dataSnapshot.getValue(Usuario.class);
                            //Salva nas preferencias o email do usuário atual
                            Preferencias preferencias = new Preferencias(LoginActivity.this);
                            preferencias.SalvarDados(usuarioRecuperado.getNome(), usuarioRecuperado.getEmail());

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    };

                    firebase.addValueEventListener(valueEventListener);
                    Toast.makeText(LoginActivity.this, "LoginActivity Realizado!", Toast.LENGTH_SHORT).show();
                    abrirTelaPrincipal();

                }
            }
        });
    }

    private void abrirTelaPrincipal() {
        Intent i = new Intent(LoginActivity.this, TelaPrincipal.class);
        startActivity(i);
        finish();
    }

    private void verificarUsuario() {
        //Verifica na inicialização do aplicativo se tem algum usuário logado
        //Caso exista, não há porque abrir a tela de login, então vai direto para tela principal.
        auth = ConfiguracaoFirebase.getFireBaseAuth();
        if (auth.getCurrentUser() != null) {
            abrirTelaPrincipal();
        }
    }

    /*Cadastra os dados dos usuario digitado nos campos no web service*/
    public boolean validarLoginWS(Login login, Context context) {
        try {
            return lc.verificaLogin(login, context);
        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
            return false;
        }
    }


}
