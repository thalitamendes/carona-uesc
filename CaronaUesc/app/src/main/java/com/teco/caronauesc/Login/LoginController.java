package com.teco.caronauesc.Login;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by estagio-nit on 19/06/18.
 */

public class LoginController {

    /*Envia os dados do login para validar no servidor*/
    public boolean verificaLogin(Login usuario, Context context) {

        try {
            return new HttpRequestAdd().execute(usuario).get();

        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
            return false;
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestAdd extends AsyncTask<Login, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Login... login) {
            LoginModel loginModel = new LoginModel();
            return loginModel.insert(login[0]);
        }
    }
}
