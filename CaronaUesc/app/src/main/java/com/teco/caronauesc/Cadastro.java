package com.teco.caronauesc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.teco.caronauesc.Usuario.Usuario;
import com.teco.caronauesc.Usuario.UsuarioController;
import com.teco.caronauesc.helper.Base64Custom;
import com.teco.caronauesc.helper.ConfiguracaoFirebase;
import com.teco.caronauesc.helper.Preferencias;

public class Cadastro extends AppCompatActivity {

    private EditText edt_nome, edt_email, edt_senha;
    private Button btn_cadastrar;
    private Button btn_cancelar;

    private Usuario usuario = new Usuario();
    private UsuarioController usuarioController;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        usuarioController = new UsuarioController();

        edt_nome = findViewById(R.id.CadastroNome);
        edt_email = findViewById(R.id.CadastroEmail);
        edt_senha = findViewById(R.id.CadastroSenha);

        btn_cadastrar = findViewById(R.id.BotaoCadastrar);
        btn_cancelar = findViewById(R.id.BotaoCancelar);

        btn_cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usuario.setNome(edt_nome.getText().toString());
                usuario.setEmail(edt_email.getText().toString());
                usuario.setSenha(edt_senha.getText().toString());

                //CadastrarUsuario();
                cadastraUsuarioWS(usuario, getBaseContext());

                Toast.makeText(Cadastro.this, "Cadastro Realizado!", Toast.LENGTH_SHORT).show();
               // finish();
            }
        });

        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void CadastrarUsuario(){
        /*Recupera uma instancia de usuário no Firebase
        * em seguida cria um usuário pelo email e senha e verifica se a operação foi um sucesso*/
        auth = ConfiguracaoFirebase.getFireBaseAuth();
        auth.createUserWithEmailAndPassword(usuario.getEmail(), usuario.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                /*Caso tenha sido um sucesso irá salvar no banco de dados os dados do usuário
                * e também salvará nas preferências o email do usuário*/
                if(task.isSuccessful()){

                    String usuarioLogado = Base64Custom.codificarBase64(usuario.getEmail());
                    usuario.setId(usuarioLogado);
                    //usuario.salvarDados();

                    Preferencias preferencias = new Preferencias(Cadastro.this);
                    preferencias.SalvarDados(usuario.getNome(), usuario.getEmail());

                    Toast.makeText(Cadastro.this, "Sucesso ao cadastrar!", Toast.LENGTH_SHORT).show();
                    abrirLoginUsuario();
                }else{

                    String errors = "";
                    /*Caso não seja um sucesso, lançará exceções
                    * aqui é onde elas serão tratadas para email já cadastrado, senha fraca etc..*/
                    try {
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        errors = "Digite uma senha mais forte, contendo mais caracteres.";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        errors = "O email digitado é inválido, digite um novo email.";
                    }catch (FirebaseAuthUserCollisionException e){
                        errors = "Email já cadastrado.";
                    }catch (Exception e){
                        errors = "Erro ao efetuar cadastro.";
                        e.printStackTrace();
                    }

                    Toast.makeText(Cadastro.this, errors, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    public void abrirLoginUsuario(){
        Intent intent = new Intent(Cadastro.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /*Cadastra os dados dos usuario digitado nos campos no web service*/
    public void cadastraUsuarioWS(Usuario usuario, Context context) {
        try {
            usuarioController.insert(usuario, context);
        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }
}
