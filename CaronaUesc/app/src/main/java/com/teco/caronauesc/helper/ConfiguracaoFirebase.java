package com.teco.caronauesc.helper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public final class ConfiguracaoFirebase {

    private static DatabaseReference firebaseDatabase;
    private static FirebaseAuth auth;

    public static DatabaseReference getFirebAse(){

        if(firebaseDatabase == null){
            firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        }

        return  firebaseDatabase;
    }

    public static FirebaseAuth getFireBaseAuth(){

        if(auth == null){
            auth = FirebaseAuth.getInstance();
        }

        return auth;
    }

}
