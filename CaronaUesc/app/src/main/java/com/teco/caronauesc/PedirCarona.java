package com.teco.caronauesc;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.teco.caronauesc.helper.Base64Custom;
import com.teco.caronauesc.helper.ConfiguracaoFirebase;
import com.teco.caronauesc.helper.Preferencias;
import com.teco.caronauesc.model.Caronas;

import java.util.ArrayList;

public class PedirCarona extends AppCompatActivity {

    private ListView listTeste;
    private ArrayList<Caronas> caronasList;
    private ArrayAdapter adapter;

    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedir_carona);

        listTeste = findViewById(R.id.listTeste);
        caronasList = new ArrayList<>();

       /*Cria o adapter que irá receber a lista de caronas
       * inicialmente um adapter padrão do Android
       * deve-se criar um novo adapter*/
        adapter = new ArrayAdapter(PedirCarona.this, android.R.layout.simple_list_item_1, caronasList);
        listTeste.setAdapter(adapter);

        /*Nas preferencias do usuário se encontra o email para não ter trabalho de recuperar
        * o email do usuário logado depois. Em seguida o converte para base64*/
        Preferencias preferencias = new Preferencias(PedirCarona.this);
        String emailRecuperado = Base64Custom.codificarBase64(preferencias.getEmail());

        /*Cria uma referencia no Firebase para o filho de "Caronas", que é onde se encontra
        * os emails dos usuário, e dentro deste nó se encontra as caronas:
        *
        *Caronas
        *       email usuario
        *                   (identificador único das caronas)
        *                                           Detalhes das caronas*/
        reference = ConfiguracaoFirebase.getFirebAse().child("Caronas").child(emailRecuperado);
        reference.addValueEventListener(new ValueEventListener() {
            /*Adiciona um Listener para quando os dados mudarem no Firebase o aplicativo ser notificado
            * e adicionar a nova carona no adapter*/
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Limpa a lista de caronas antes de setar uma nova
                caronasList.clear();
                for (DataSnapshot dados : dataSnapshot.getChildren()) {
                    /*Percorre os dados das caronas e cria um objeto espelhado
                    * na classe Caronas.java*/
                    Caronas caronas = dados.getValue(Caronas.class);
                    caronasList.add(caronas);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
