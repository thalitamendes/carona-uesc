package com.teco.caronauesc.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.teco.caronauesc.model.Caronas;

public class Preferencias {

    private Context context;
    private final String NOME_ARQUIVO = "caronaUesc.preferencias";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private final String CHAVE_EMAIL = "LoginUsuário";
    private final String CHAVE_NOME = "EmailUsuario";

    public Preferencias(Context contexto) {
        context = contexto;
        preferences = context.getSharedPreferences(NOME_ARQUIVO, 0);
        editor = preferences.edit();
    }


    public void SalvarDados( String nome ,String email){

        editor.putString(CHAVE_EMAIL, email);
        editor.putString(CHAVE_NOME, nome);
        editor.commit();
    }

    public String getEmail(){
        return preferences.getString(CHAVE_EMAIL, null);
    }

    public String getNome(){
        return preferences.getString(CHAVE_NOME, null);
    }

}
